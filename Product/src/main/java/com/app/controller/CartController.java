package com.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.model.Cart;
import com.app.service.CartService;

@CrossOrigin(origins="http://localhost:4200")

@RestController

@RequestMapping("/Cart")


public class CartController {
	
	@Autowired
	private CartService cartService;
	
	@GetMapping({"/addToCart/{id}"})
	public Cart addToCart(@PathVariable(name="id")Integer id) {
		return cartService.addToCart(id);
	}
	
	@DeleteMapping({"/deleteCartItem/{id}"})
	
	public void deleteCartItem(@PathVariable(name="id")Integer id){
		cartService.deleteCartItem(id);
		
	}
	
	@GetMapping({"/getCartDetails"})
	public List<Cart> getCartDetails() {
		
		return cartService.getCartDetails();
		
	}
	
	
	
	
	
	
	//sidd
	
	
	

}
