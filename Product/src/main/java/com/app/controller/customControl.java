package com.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.app.model.Customer;
import com.app.repository.ProductRepo;
import com.app.service.ProductService;

@CrossOrigin(origins="http://localhost:4200")
@RestController
@RequestMapping("/products")

public class customControl {
	@Autowired
	ProductService productService; 
	
	@Autowired
	ProductRepo productRepo;
	
	@PostMapping("/addProduct")
	public Customer addData(@RequestBody Customer pro) {
		return this.productService.addProduct(pro);		
	}
	
	 @GetMapping("/getProduct")
	    public List<Customer> getAllProducts() {
	        return productService.getAllProducts();
	    }
	 @DeleteMapping("/delProduct/{id}")
	    public ResponseEntity<String> deleteProduct(@PathVariable("id") Integer id) {
	        productService.deleteProduct(id);
	        return new ResponseEntity<>("Product deleted successfully", HttpStatus.OK);
	    }
	 @GetMapping("/getProducts/{id}")
	    public Customer getProductById(@PathVariable("id") Integer id){
	        return productService.getProductById(id);
	    }
	 @PutMapping("/editProducts/{id}")
	    public ResponseEntity<Customer> updateProduct(
	         @PathVariable("id") Integer id,
	            @RequestBody Customer updatedProductData
	    ) {
	        Customer updatedProduct = productService.updateProduct( id, updatedProductData);
	        
	        return new ResponseEntity(updatedProduct, HttpStatus.OK);
	    }
	 @GetMapping({"/getProductDetail/{isSingleProductCheckout}/{id}"})
	 public List<Customer> getProductDetail(@PathVariable(name="isSingleProductCheckout")boolean isSingleProductCheckout,@PathVariable(name="id") Integer id) {
		 return productService.getProductDetail(isSingleProductCheckout, id);
		 
	 }
//	 "
	
	 
	
	

}
