package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.model.OrderInput;
import com.app.service.OrderDetailService;

@CrossOrigin(origins="*")

@RestController
@RequestMapping("/Order")

public class OrderDetailController {
	
	@Autowired
	private OrderDetailService orderDetailService;
	
	@PostMapping({"/placeOrder/{isCartCheckout}"})
	
	public void placeOrder(@PathVariable(name="isCartCheckout")boolean isCartCheckout,@RequestBody OrderInput orderInput) {
		orderDetailService.placeOrder(orderInput,isCartCheckout);
		
	}

}
