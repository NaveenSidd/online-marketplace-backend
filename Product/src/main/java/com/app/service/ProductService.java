package com.app.service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import com.app.model.Cart;
import com.app.model.Customer;
import com.app.repository.CartDao;
import com.app.repository.ProductRepo;


@Service
public class ProductService {
	@Autowired
	ProductRepo productrepo;
	
	@Autowired
	CartDao cartDao;
	
	public Customer addProduct(Customer reg) {
		return this.productrepo.save(reg);
	}
	public List<Customer> getAllProducts() {
        return  productrepo.findAll();
    }
	 public void deleteProduct(Integer id) {
	        productrepo.deleteById(id); 
	}
	 public Customer getProductById(Integer id) {
	        return this.productrepo.findById(id).orElse(null);
	    }
	 
	 
	 public Customer updateProduct(Integer productId, Customer updatedProductData) {
	        Customer product = productrepo.findById(productId)
	                .orElseThrow(() -> new NoSuchElementException("Product not found"));

	        // Update fields as needed
	        product.setType(updatedProductData.getType());
	        product.setName(updatedProductData.getName());
	        product.setPrice(updatedProductData.getPrice());
	        product.setImage(updatedProductData.getImage());

	        return productrepo.save(product);
	    }
	 public List<Customer> getProductDetail(boolean isSingleProductCheckout, Integer id) {
		 if(isSingleProductCheckout && id!=0) {
			 List<Customer> list=new ArrayList<>();
			 Customer customer=productrepo.findById(id).get();
			 list.add(customer);
			 return list;
		 }
		 else {
			 List<Cart> carts=cartDao.findAll();
			 return carts.stream().map(x -> x.getCustomer()).collect(Collectors.toList());
			 
			 
		 }
		 
	 }
	
}
