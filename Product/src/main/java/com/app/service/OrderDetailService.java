package com.app.service;


import java.util.List;

//import org.hibernate.mapping.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.Cart;
import com.app.model.Customer;
import com.app.model.OrderDetail;
import com.app.model.OrderInput;
import com.app.model.OrderProductQuantity;
import com.app.repository.CartDao;
import com.app.repository.OrderDetailDao;
import com.app.repository.ProductRepo;

@Service

public class OrderDetailService {
	private static final String ORDER_PLACED="placed";
	@Autowired
	private OrderDetailDao orderDetailDao;
	
	@Autowired
	private ProductRepo productRepo;
	
	@Autowired
	private CartDao cartDao;
	
	public void placeOrder(OrderInput orderInput,boolean isCartCheckout) {
		List<OrderProductQuantity> productQuantityList=orderInput.getOrderProductQuantityList();
		
		for(OrderProductQuantity o:productQuantityList) {
			Customer customer= productRepo.findById(o.getId()).get();
			int orderAmount;
			OrderDetail orderDetail=new OrderDetail(
					orderInput.getFullName(),
					orderInput.getFullAddress(),
					orderInput.getContactNumber(),
					orderInput.getAlternateContactNumber(),
					ORDER_PLACED,
					orderAmount=customer.getPrice()*o.getQuantity(),
					customer
					
					);
			if(!isCartCheckout) {
				List<Cart> carts=cartDao.findAll();
				carts.stream().forEach(x -> cartDao.deleteById(x.getCartId()));
			}
			
			orderDetailDao.save(orderDetail);
			
		}
		
	}

}
