package com.app.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.Cart;
import com.app.model.Customer;
import com.app.repository.CartDao;
import com.app.repository.ProductRepo;

@Service

public class CartService {
	
	@Autowired
	private CartDao cartDao;
	
	@Autowired
	private ProductRepo productRepo;
	
	public void deleteCartItem(Integer id) {
		cartDao.deleteById(id);;
		
	}
	
	public Cart addToCart(Integer id) {
		List<Cart> cartList=cartDao.findAll();
		List<Cart>filteredList=cartList.stream().filter(x ->x.getCustomer().getId() == id).collect(Collectors.toList());
		
		if(filteredList.size()>0) {
			return null;
		}
		
		Customer customer=productRepo.findById(id).get();
		
		if(customer != null) {
			
			Cart cart=new Cart(customer);
			return cartDao.save(cart);
			
			
		}
		return null;	
	}
	public List<Cart> getCartDetails(){
		return cartDao.findAll();
	}

}
