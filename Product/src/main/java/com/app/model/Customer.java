package com.app.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name="Products")
public class Customer {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="Type",nullable=false,unique=true)
	private String type;
	
	@Column(name="Name",nullable=false,unique=true)
	private String name;
	
	@Column(name="Price",nullable=false,unique=true)
	private Integer price;
	
	@Column(name="Images",nullable=false,unique=true)
	private String image;
	

}
