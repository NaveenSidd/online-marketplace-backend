package com.app.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.Model.Registeration;

import com.app.service.LoginService;
import com.app.service.StudentService;


@RestController
@CrossOrigin(origins="http://localhost:4200")
@RequestMapping("/student")

public class StudentController {
	
	@Autowired
	StudentService studentService; 
	
	 @Autowired
	 LoginService loginService;
	
	@PostMapping("/Registeration")
	public Registeration addData(@RequestBody Registeration regi) {
		
		return this.studentService.addStudentService(regi);	
		
	    }
	 
	@PostMapping("/user")
	public ResponseEntity userlogin(@RequestBody Registeration registeration) {
		
		boolean loginStatus=loginService.loginUser(registeration);
		
		if(loginStatus==true) {
			return new ResponseEntity(loginStatus,HttpStatus.OK);
		}
		
		return new ResponseEntity(HttpStatus.UNAUTHORIZED) ;
	}

}
