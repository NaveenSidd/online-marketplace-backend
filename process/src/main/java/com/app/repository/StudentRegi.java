package com.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.Model.Registeration;


public interface StudentRegi extends JpaRepository<Registeration, Integer> {
	
	Registeration findByEmail(String email);
}
