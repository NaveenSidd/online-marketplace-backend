package com.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.app.Model.Registeration;
import com.app.repository.StudentRegi;

@Service
public class LoginService {

	@Autowired
	private JavaMailSender javaMailSender;

	@Autowired
	StudentRegi studentRegi;
	
	public void sendMail(String fromMail, String toEmail, String heading, String body) {
		SimpleMailMessage mailMessage = new SimpleMailMessage();
		mailMessage.setFrom(fromMail);
		mailMessage.setTo(toEmail);
		mailMessage.setSubject(heading);
		mailMessage.setText(body);
		
		javaMailSender.send(mailMessage);
	}

	boolean status = false;

	public boolean loginUser(Registeration registeration) {

		Registeration reg = studentRegi.findByEmail(registeration.getEmail());

		boolean loginstatus;

		if (reg != null) {
			if (reg.getPassword().equals(registeration.getPassword())) {

				status = true;
				loginstatus = status;
				status = false;
				
				String getEnteredMail = registeration.getEmail();
			    
				sendMail("cnsiddharth3333@gmail.com", getEnteredMail, "Logged In", "Explore Buy and Sell Products");

				return loginstatus;
			}
		}
		
		
		
		return status;
		
	}


}
