package com.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.Model.Registeration;
import com.app.repository.StudentRegi;

@Service

public class StudentService {
	
	@Autowired
	StudentRegi studentRegi;
	
	public Registeration addStudentService(Registeration reg) {
		return this.studentRegi.save(reg);
		
	}

}
